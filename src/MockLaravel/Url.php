<?php

namespace Encore\Admin\MockLaravel;


class Url
{
    protected $path,$parmeters,$secure;
    public function __construct($path = null, $parameters = [], $secure = null)
    {
        $this->path = $path;
        $this->parmeters = $parameters;
        $this->secure = $secure;
    }


    //ok
    public function isValidUrl($path)
    {
        if (! preg_match('~^(#|//|https?://|(mailto|tel|sms):)~', $path)) {
            return filter_var($path, FILTER_VALIDATE_URL) !== false;
        }

        return true;
    }

    //TODO NO
    public function getBaselarUrl()
    {
        return  config('app.url');
    }

    //TODO no
    public function fulllarUrl()
    {

        return '/';
    }

    //TODO NO
    public function previous()
    {
        return '/';
    }

    //TODO
    public function current()
    {
        return request()->getPathInfo();
    }

}

